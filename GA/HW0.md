Implement a deterministic and a heuristic search method.
Test and compare them using 4 benchmark functions.
You can find benchmark functions [here](http://www-optima.amp.i.kyoto-u.ac.jp/member/student/hedar/Hedar_files/TestGO_files/Page364.htm)
One of the functions should be Rastrigin's Function.

The functions should be tested using the following numbers of dimensions:
2, 5, 20.

In the case of heuristic search methods, a proper sample size (minimally 30) should be used.

Write a report detailing and explaining your motivation, method, results, and draw conclusions.

The report should be written in LaTeX, or .md or .txt .